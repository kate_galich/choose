package ca.on.conestogac.kgalich.choose;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;


public class MainActivity extends AppCompatActivity {

    private Button buttonOne;
    private Button buttonTwo;
    private TextView textViewResult;
    private ImageView imageViewLeft;
    private ImageView imageViewRight;

    private SharedPreferences sharedPref;
    private int userChoice;
    private int compChoice;
    private static final int DEFAULT_USER_CHOICE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.Theme_Choose);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonOne = findViewById(R.id.buttonOne);
        buttonTwo = findViewById(R.id.buttonTwo);
        textViewResult = findViewById(R.id.textViewResult);
        imageViewLeft = findViewById(R.id.imageViewLeft);
        imageViewRight = findViewById(R.id.imageViewRight);

        buttonOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageViewRight.setImageResource(R.drawable.ic_one);
                compChoice = (int) Math.round(Math.random());
                userChoice = 0;


                switch (compChoice) {
                    case 0:
                        imageViewLeft.setImageResource(R.drawable.ic_one);
                        break;
                    case 1:
                        imageViewLeft.setImageResource(R.drawable.ic_two);
                        break;
                }

                if (compChoice == 0) {
                    textViewResult.setText(R.string.youWin);
                } else {
                    textViewResult.setText(R.string.compWins);
                }
            }
        });

        buttonTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageViewRight.setImageResource(R.drawable.ic_two);
                compChoice = (int) Math.round(Math.random());
                userChoice = 1;

                switch (compChoice) {
                    case 0:
                        imageViewLeft.setImageResource(R.drawable.ic_one);
                        break;
                    case 1:
                        imageViewLeft.setImageResource(R.drawable.ic_two);
                        break;
                }

                if (compChoice == 0) {
                    textViewResult.setText(R.string.compWins);
                } else {
                    textViewResult.setText(R.string.youWin);
                }
            }
        });

        sharedPref = getSharedPreferences("lastChoice", MODE_PRIVATE);
    }

    @Override
    protected void onPause() {
        Editor ed = sharedPref.edit();
        ed.putInt("user_choice", userChoice);
        ed.putInt("comp_choice", compChoice);
        ed.commit();

        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        userChoice = sharedPref.getInt("user_choice", DEFAULT_USER_CHOICE);
        compChoice = sharedPref.getInt("comp_choice", DEFAULT_USER_CHOICE);
        if (userChoice == DEFAULT_USER_CHOICE) {
            return;
        }

        if (userChoice==0){
            imageViewRight.setImageResource(R.drawable.ic_one);
        }
        else {
            imageViewRight.setImageResource(R.drawable.ic_two);
        }

        if (compChoice==0){
            imageViewLeft.setImageResource(R.drawable.ic_one);
        }
        else{
            imageViewLeft.setImageResource(R.drawable.ic_two);
        }

        if(userChoice==compChoice){
            textViewResult.setText(R.string.youWin);
        }
        else{
            textViewResult.setText(R.string.compWins);
        }
    }
}